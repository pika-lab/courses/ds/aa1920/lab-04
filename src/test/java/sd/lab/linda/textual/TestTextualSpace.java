package sd.lab.linda.textual;

import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sd.lab.linda.Agent;
import sd.lab.test.ConcurrentTestHelper;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestTextualSpace {
    
    private static ExecutorService executor;

    @BeforeClass
    public static void setUpUnit() throws Exception {
        executor = Executors.newSingleThreadExecutor();
    }
    
    private TextualSpace tupleSpace;
    private ConcurrentTestHelper test;
    private Random rand;
    
    @Before
    public void setUp() throws Exception {
        tupleSpace = TextualSpace.of(executor);
        test = new ConcurrentTestHelper();
        rand = new Random();
    }
    
    @Test
    public void testInitiallyEmpty() throws Exception {
    	test.setThreadCount(1);
    	
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.getSize(), 0, "The tuple space must initially be empty");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    
    @Test
    public void testReadSuspensiveSemantics() throws Exception {
    	test.setThreadCount(1);
    	
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertBlocksIndefinitely(tupleSpace.rd(".*"), "A read operation should block if no tuple matching the requested template is available");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    @Test
    public void testTakeSuspensiveSemantics() throws Exception {
    	test.setThreadCount(1);
    	
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertBlocksIndefinitely(tupleSpace.in(".*"), "A take operation should block if no tuple matching the requested template is available");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    
    
    @Test
    public void testWriteGenerativeSemantics() throws Exception {
    	test.setThreadCount(1);
    	
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.getSize(), 0, "The tuple space must initially be empty");
                test.assertEquals(tupleSpace.out("foo"), StringTuple.of("foo"), "A write operation eventually return the same tuple it received as argument");
                test.assertEquals(tupleSpace.getSize(), 1, "After a tuple was written, the tuple space size should increase");
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    @Test
    public void testReadIsIdempotent() throws Exception {
    	test.setThreadCount(2);
    	
        final StringTuple tuple = StringTuple.of("foo bar");
        
        final Agent bob = new Agent("Bob") {
            
            @Override 
            protected void loop() throws Exception {
                for (int i = rand.nextInt(10) + 1; i >= 0; i--) {
                    test.assertEquals(tupleSpace.rd(".*?foo.*"), tuple);
                }
                test.assertEquals(tupleSpace.rd(".*?bar.*"), tuple);
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEventuallyReturns(tupleSpace.out(tuple));
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
    }
    
    @Test
    public void testTakeIsNotIdempotent2() throws Exception {
    	test.setThreadCount(2);
    	
        final StringTuple tuple = StringTuple.of("foo bar");
        
    	final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.out(tuple));
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        };
        
        final Agent bob = new Agent("Bob") {
            
            @Override 
            protected void loop() throws Exception {
            	final Future<StringTuple> toBeTaken = tupleSpace.in(".*?foo.*");
            	alice.start();
                test.assertEquals(toBeTaken, tuple);
                test.assertBlocksIndefinitely(tupleSpace.in(".*?bar.*"));
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
    }
    
    @Test
    public void testTakeIsNotIdempotent1() throws Exception {
    	test.setThreadCount(2);
    	
        final StringTuple tuple = StringTuple.of("foo bar");
        
        final Agent bob = new Agent("Bob") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.in(".*?foo.*"), tuple);
                test.assertBlocksIndefinitely(tupleSpace.in(".*?bar.*"));
                stop();
            }

            @Override 
            protected void onEnd() {
                test.done();
            }
 
        };
        
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.out(tuple));
            	bob.start();
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
    }
    
    @Test
    public void testAssociativeAccess() throws Exception {
    	test.setThreadCount(3);
    	
        final StringTuple tuple4Bob = StringTuple.of("recipient: bob");
        final StringTuple tuple4Carl = StringTuple.of("recipient: carl");
        
        final Agent carl = new Agent("Carl") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.rd(".*?carl.*"), tuple4Carl);
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        final Agent bob = new Agent("Bob") {
            
            @Override 
            protected void loop() throws Exception {
                test.assertEquals(tupleSpace.rd(".*?bob.*"), tuple4Bob);
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.out(tuple4Bob));
            	test.assertEventuallyReturns(tupleSpace.out(tuple4Carl));
                
                test.assertOneOf(tupleSpace.in("recipient:.*"), tuple4Bob, tuple4Carl);
                test.assertOneOf(tupleSpace.in("recipient:.*"), tuple4Bob, tuple4Carl);
                
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
        bob.await();
        carl.await();
    }
    
    @Test
    public void testGetSize() throws Exception {
    	test.setThreadCount(1);
        
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEquals(tupleSpace.getSize(), 0);
            	test.assertEventuallyReturns(tupleSpace.out("a"));
            	test.assertEquals(tupleSpace.getSize(), 1);
            	test.assertEventuallyReturns(tupleSpace.out("a"));
            	test.assertEquals(tupleSpace.getSize(), 2);
            	test.assertEventuallyReturns(tupleSpace.out("a"));
            	test.assertEquals(tupleSpace.getSize(), 3);
            	
            	test.assertEventuallyReturns(tupleSpace.in("a"));
            	test.assertEquals(tupleSpace.getSize(), 2);
                
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
    @Test
    public void testGetAll() throws Exception {
    	test.setThreadCount(1);
    	
    	final MultiSet<StringTuple> expected = new HashMultiSet<>(Arrays.asList(
    				StringTuple.of("b"),
    				StringTuple.of("c"),
    				StringTuple.of("a"),
    				StringTuple.of("b")
    			));
        
        final Agent alice = new Agent("Alice") {
            
            @Override 
            protected void loop() throws Exception {
            	test.assertEventuallyReturns(tupleSpace.out("a"));
            	test.assertEventuallyReturns(tupleSpace.out("b"));
            	test.assertEventuallyReturns(tupleSpace.out("b"));
            	test.assertEventuallyReturns(tupleSpace.out("c"));
            	
            	test.assertEquals(tupleSpace.get(), expected);
                
                stop();
            }
            
            @Override 
            protected void onEnd() {
                test.done();
            }
 
        }.start();
        
        test.await();
        alice.await();
    }
    
}
